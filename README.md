# Before Running

1. First make sure you have installed `sshpass` in the local machine.

```
    sudo apt install sshpass
```

2. Install dependencies

```
    npm install
```

3. Make sure you have installed `spammer` repo in all the remote machines.

```
    repo link : https://gitlab.com/liberdus/spam-client
```

3. Create a `hosts.json` file in the project root and add hosts to it like this example.

```
[
    {
        "host": "host_ip",
        "username": "your_username",
        "password": "your_password",
        "command": "spammer spam --type create --d 10 --r 10 --a 100 --monitor http://208.110.82.50:3000/api/report --q 20 --output accounts.json"
    }
]

```

- command field is optional; if added, it will be run as soon as ssh connected!

4. Run the program with command

```
    node orchestrator.js (or) npm run app
```

5. Navigate the related host's display

- With Tab key.
- For quitting this run, as usual q or esc keys

6. To scroll through the host's terminal page, use

- Ctrl+Up key for scroll up
- Ctrl+Down key for scroll down

7. For the textBox on the bottom

- Press `Ctrl+A` key for focus
- Press `Enter` key after entering command

8. In Spam Summary Info page,

- Click it for focus
- Use 'Up' or 'Down' key for scrolling
