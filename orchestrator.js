const blessed = require("blessed");
const XTerm = require("blessed-xterm")
const { resolve } = require('path');
const Tail = require('tail').Tail;
const fs = require('fs');


// Create a screen object.
const screen = blessed.screen({
    smartCSR: true,
});

screen.title = "SPAM CLIENT ORCHESTRATOR";

screen.focusable = true;

// Quit on Escape
screen.key(["escape"], function (ch, key) {
    return process.exit(0);
});

const spamFileName = 'spamming.txt'

fs.writeFileSync(spamFileName, '')

const tail = new Tail(spamFileName, "\n", {}, true);
let totalSuccess = 0;
let totalFail = 0;
let totalInject = 0;

const summaryBox = blessed.log({
    parent: screen,
    right: 0,
    top: 0,
    height: '93%',
    width: Math.floor(screen.width / 7) * 3 - 1,
    border: 'line',
    label: 'Spam Summary Info',
    padding: 1,
    keys: true,
    vi: true,
    alwaysScroll: true,
    scrollable: true,
    clickable: true,
    scrollbar: {
        style: {
            bg: 'yellow'
        }
    },
    style: {
        fg: 'white',
        bg: 'default',
        border: {
            fg: 'cyan',
            bold: true
        },
        selected: {
            bg: 'green',
            fg: 'white'
        },
        focus: { border: { fg: "green" } },
    }
});
tail.on("line", function (data) {
    if (data.includes('txSent'))
        summaryBox.pushLine(data)
    if (data.includes('totalInject')) {
        const split = data.match(/\d+/g)
        if (split.length > 2) {
            totalInject += parseInt(split[0])
            totalSuccess += parseInt(split[1])
            totalFail += parseInt(split[2])
            summaryBox.pushLine(`{totalTxInject: ${totalInject}, totalTxSuccess: ${totalSuccess}, totalTxFail: ${totalFail}}`)
        }
    }

});

tail.on("error", function (error) {
    summaryBox.pushLine(data)
});

const hostFile = resolve('./hosts.json')

let hosts = [];

try {
    hosts = require(hostFile)
} catch (e) {
    console.log('Host.json file not found!')
    console.log(e)
    return
}


let selected_host = 0;
let terminal = []

const textbox = blessed.list({
    parent: screen,
    height: '93%',
    width: Math.floor(screen.width / 6),
    scrollable: true,
    border: 'line',
    label: 'Remote Servers',
    top: 0,
    bottom: 0,
    left: 0,
    padding: 1,
    clickable: false,
    style: {
        fg: 'white',
        bg: 'default',
        border: {
            fg: 'cyan',
            bold: true
        },
        selected: {
            bg: 'green',
            fg: 'white'
        },
        focus: { border: { fg: "green" } },
    }
});

const opts = {
    shell: process.env.SHELL || "sh",
    args: [],
    env: process.env,
    cwd: process.cwd(),
    cursorType: "block",
    border: "line",
    scrollback: 100000,
    clickable: true,
    style: {
        fg: "white",
        bg: "default",
        border: { fg: "cyan", bold: true },
        focus: { border: { fg: "green" } },
        scrolling: { border: { fg: "red" } }
    }
}


const commandBox = blessed.textbox({
    parent: screen,
    height: 4,
    bottom: 0,
    label: "Enter command to run on all the hosts",
    border: "line",
    value: hosts.find(x => typeof (x.command) === 'string').command,
    style: {
        border: {
            fg: "white",
        },
    },
});


function drawBoard() {
    let num = 0;
    while (num < hosts.length) {
        textbox.pushItem(`${hosts[num].host}`)

        terminal[num] = new XTerm(Object.assign({}, opts, {
            left: Math.floor(screen.width / 6) + 1,
            top: 0,
            width: Math.floor(screen.width / 7) * 3,
            height: screen.height / 2 - 3,
            label: " CLI Interface  :  " + hosts[num].host + " "
        }))


        const password = hosts[num].password.replace(/([^a-zA-Z0-9])/g, "\\$1");
        const ssh_command = ` sshpass -p ${password} ssh -o stricthostkeychecking=no ${hosts[num].username}@${hosts[num].host} | tee -a ${spamFileName}\n`
        terminal[num].write('\n');
        terminal[num].injectInput(ssh_command);
        if (hosts[num].command) {
            terminal[num].injectInput(hosts[num].command + '\n');
        }

        num++;

        if (num < hosts.length) {
            textbox.pushItem(`${hosts[num].host} `)
            terminal[num] = new XTerm(Object.assign({}, opts, {
                left: Math.floor(screen.width / 6) + 1,
                top: screen.height / 2 - 1,
                width: Math.floor(screen.width / 7) * 3,
                height: screen.height / 2 - 3,
                label: " CLI Interface  :  " + hosts[num].host + " "
            }))

            const password = hosts[num].password.replace(/([^a-zA-Z0-9])/g, "\\$1");
            const ssh_command = ` sshpass -p ${password} ssh -o stricthostkeychecking=no ${hosts[num].username}@${hosts[num].host} | tee -a ${spamFileName}\n`
            terminal[num].write('\n');
            terminal[num].injectInput(ssh_command);
            if (hosts[num].command) {
                terminal[num].injectInput(hosts[num].command + '\n');
            }

            num++;
        }
    }
    screen.append(terminal[selected_host])
    if (selected_host < hosts.length - 1) {
        screen.append(terminal[selected_host + 1])
    }
    terminal[selected_host].focus()
    textbox.select(selected_host);

    screen.key(["tab"], (ch, key) => {
        if (selected_host % 2 !== 0) {
            screen.remove(terminal[selected_host - 1])
            screen.remove(terminal[selected_host])
        }
        if (selected_host < hosts.length - 1)
            selected_host++
        else
            selected_host = 0
        if (selected_host % 2 === 0) {
            screen.append(terminal[selected_host])
            if (selected_host < hosts.length - 1) {
                screen.append(terminal[selected_host + 1])
            }
        }
        terminal[selected_host].focus()
        textbox.select(selected_host)
        screen.render();
    })

    terminal.forEach((term) => {
        term.key("C-down", () => {
            if (!term.scrolling)
                term.scroll(0)
            const n = Math.max(1, Math.floor(term.height * 0.10))
            term.scroll(+2)
            if (Math.ceil(term.getScrollPerc()) === 100)
                term.resetScroll()
        })
        term.key("C-up", () => {
            if (!term.scrolling)
                term.scroll(0)
            const n = Math.max(1, Math.floor(term.height * 0.10))
            term.scroll(-2)
            if (Math.ceil(term.getScrollPerc()) === 100)
                term.resetScroll()
        })
    })

    textbox.show();
    textbox.select(selected_host);


    screen.key("C-a", async () => {
        commandBox.readInput()
    });

    commandBox.key("enter", () => {
        for (let i = 0; i < hosts.length; i++) {
            let command = commandBox.getValue();
            terminal[i].injectInput('\x03\n');
            terminal[i].injectInput(command + '\n')
        }
    });

    summaryBox.show();
    summaryBox.on("click", () => {
        summaryBox.focus();
    });

    screen.render();
}

drawBoard();

